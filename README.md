# edu-java-health
Java backend application for educational purposes

## usage

To run the project:

```shell
## open command line and run
$ gradle bootRun
```

Try to run `chomd +x gradlew` if you deal with some permissions issues.

Open a browser windown and paste: `http://localhost:8888/swagger-ui.html#/Appointment`.

If the following print appears, then everything is ok.
![](Untitled.png)

## Change port

The default port is `8888`, if you want to change it,  in `gradle/src/main/java/pt/dlt/health/resources/application.properties` file, line `server.port = <your port>`